// Initialize variables
let balance = 0;
let earnings = 0;
 let loan = 0;// = document.getElementById("loan");
// if(loan!= 0){

// }
// let loan = document.getElementById("loan")
// loan.innerHTML = 0;
let loanTaken = false;

// DOM elements
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const earningsElement = document.getElementById("earnings");
const balanceButton = document.getElementById("balance-button");
const loanButton = document.getElementById("loan-button");
const workButton = document.getElementById("work-button");
const bankButton = document.getElementById("bank-button");
const repayLoanButton = document.getElementById("repayLoan-button");
const laptopSelect = document.getElementById("laptop-select");
const laptopDetails = document.getElementById("laptop-details");
const lpListElement= document.getElementById("LP-list")// to test
const lpInfoElement= document.getElementById("LP-Info")// to test
const drinksListElement= document.getElementById("drink-list")// to test
const btnGetInfoLP= document.getElementById( "Get-LP-Info")
const BuyNowButton= document.getElementById( "Buy-Now-Button")
const buttonLoan =document.getElementById("buttonLoan")



// Update balance  in DOM
function updateBalance() {
    balanceElement.textContent = balance.toFixed(0);
}
// Update loans  in DOM
function updateLoan() {
    //  loan = document.getElementById("loan");
     loanElement.textContent = loan //.toFixed(2);
}

function updateEarnings() {
    earningsElement.textContent = earnings.toFixed(0);
}
// loan button
loanButton.addEventListener("click", () => {
    if (loan < 1 && balance>0) { // loan taken !loanTaken && 
        const loanAmount = balance *2;
        loan += loanAmount;
        loanTaken = false; // change this to true at the end for chang bolegen.
        updateBalance();
        updateLoan();
    } else {
        alert("You can't take more than one loan or take a loan with zero balance.");
    }
});
workButton.addEventListener("click", () => {
    earnings += 100;
    updateEarnings();
});

// Deposit to bank button

bankButton.addEventListener("click", () => {
    if (loan <1) {
    balance += earnings;
    earnings = 0;
   // loanTaken = false;
    updateBalance();
    updateEarnings();
    updateLoan();
} else {
    balance += earnings*0.9;
    loan -= earnings*0.1;
    earnings = 0;
   // loanTaken = false;
    updateBalance();
    updateEarnings();
    updateLoan();
}}
);
// Repay loan button 
repayLoanButton.addEventListener("click", () => {
    if (loan >earnings) {
    loan -= earnings;
    earnings = 0;
   // loanTaken = false;
    updateBalance();
    updateEarnings();
    updateLoan();
} else {
    balance += earnings-loan;
    loan =0;
    earnings = 0;   
   // loanTaken = false;
    updateBalance();
    updateEarnings();
    updateLoan();
}}
);
function ShowRepayBtn ()  {
    userSection.style.display ="noen"
}

const API ="https://hickory-quilled-actress.glitch.me/computers"
let LP = []
async function getLP () {
    const respLP = await fetch (API)
 const json= await respLP.json()
   LP =json 
    console.log(LP)
    renderLP(LP)
}

const APItest ="https://hickory-quilled-actress.glitch.me/computers"
let LPtest = []
async function getLPtest () {
    const respLP = await fetch (APItest)
 const json= await respLP.json()
   LP =json 
    console.log(["test",LP]);
    return await LP 
}
// const test1 = getLPtest()
// console.log(["test2",test1[1]]) 

getLP()

function renderLP () {
    lpListElement.innerHTML=''
    for (const CurrentLP of LP){
    const newElement=document.createElement('li')
newElement.innerText =`${CurrentLP.title} - ${CurrentLP.price} kr  : ${CurrentLP.description} `
lpListElement.appendChild(newElement)
     } }

    //  btnGetInfoLP.addEventListener("click",GetLPInfo)

     function GetLPInfo(){
        DataLP()     }

     function DataLP () {
        if(!LP) return
        for (const CurrentLPS of LP){
            lpInfoElement.innerText =`${CurrentLPS.title} - ${CurrentLPS.price}`
         } }
    
//##############################################################################
// henter API data og retunerer dette som laptops

async function fetchLaptopsJSON() {
    const response = await fetch(APItest);
    const Laptops = await response.json();
    return Laptops;
  }

fetchLaptopsJSON().then(Laptops => {
    Laptops
    // console.log(["testepsne1",Laptops]); 
// Oppretter options for laptopSelect Som brukes lengre ned i scriptet
    Laptops.forEach(Laptops => {
        const option = document.createElement("option");
        option.value = Laptops.id;
        option.textContent = Laptops.title;
        option.price = Laptops.price;
        option.description = Laptops.description;
        option.image = Laptops.image;
        laptopSelect.appendChild(option); 
    });
    
});


// Display selected laptop details
laptopSelect.addEventListener("change", () => {

    if (laptopSelect) {
        laptopDetails.innerHTML = `
            <h3>${laptopSelect.options[laptopSelect.selectedIndex].textContent}</h3>
            <p>Price: kr ${laptopSelect.options[laptopSelect.selectedIndex].price}</p>
            <p>${laptopSelect.options[laptopSelect.selectedIndex].description}</p>
            <img src="https://hickory-quilled-actress.glitch.me/${laptopSelect.options[laptopSelect.selectedIndex].image}" alt="pc" width="200" height="200">
        `;
    }
});

//###################################################
// hide/show repaybutton
loanButton.addEventListener("click", function hideShowLoanbutton ()  {
    if( loan>0 ){
        repayLoanButton.style.display="block";
        buttonLoan.style.display="none";
        loanButton.style.display="none"
    } else { repayLoanButton.style.display="none"}
})

// hide/show loanbutton
workButton.addEventListener("click", function hideShowLoanbutton ()  {
    if( loan>0 ){
        loanButton.style.display="none";
        buttonLoan.style.display="none";
        repayLoanButton.style.display="block";
    } else { loanButton.style.display="block";
        buttonLoan.style.display="block";
        repayLoanButton.style.display="none";

    }
})
loanButton.addEventListener("click", function hideShowLoanbutton ()  {
    if( loan>0 ){
        loanButton.style.display="none";
        buttonLoan.style.display="none"
    } else { loanButton.style.display="block";
        buttonLoan.style.display="block"
        ;
    }
} )


//Buy Now
BuyNowButton.addEventListener("click", () => {
    
    //###############################################################
    //  allerede definert laptop-select sine options, derfra må vi bare hente den som er slected altså selectedIndex
    var laptopselected = document.getElementById("laptop-select");
    var value = laptopselected.options[laptopselected.selectedIndex].value;
    var text = laptopselected.options[laptopselected.selectedIndex].text;
    var price = laptopselected.options[laptopselected.selectedIndex].price;

    //###############################################################
    if (price<=balance+loan) { // If ballanse ok 
        
        alert("you are now the owner of the new laptop! .");
        if ( loan>=price){
        loan-=price;}
        else {  balance =  parseInt( loan)+ parseInt(balance)- parseInt(price);
            loan=0;
        }
    
        updateLoan()
        updateBalance()

    } else {
        alert("You cannot afford the laptop");
    }
});

// Funksjonen blir trigget av knapp i index html Funksjonen henter ut promt data og lager til innerHTML variabelen loan
function getLoan(){
    const  amount = prompt("Enter loan amount:")
if( amount<=2*balance && loan<=0)
    {
    loan=amount
    // console.log(loan)
    // console.log(amount)
    // const loanElement = document.getElementById("loan")
    updateLoan();
    // loan.innerHTML = loan;
} else { alert("Balance is to low or you cant  loan more then  2 times your balance or have more then one loan at the time")}};
// Initial setup
updateBalance();
updateEarnings();
updateLoan();
